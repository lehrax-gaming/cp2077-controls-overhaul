// inspired by original work of djkovrik: https://github.com/djkovrik/CP77Mods/blob/master/src/Limited%20HUD%20-%20Simple%20HUD%20Toggle/r6/scripts/LHUD/misc/hudSimpleToggleStandalone.reds
public class QuestToggleEvent extends Event {}

@addMethod(QuestTrackerGameController)
protected cb func OnQuestToggleEvent(evt: ref<QuestToggleEvent>) -> Bool {
  let trackedQuest: wref<JournalEntry> = this.m_journalManager.GetTrackedEntry();

  if this.m_journalManager.GetParentEntry(
    this.m_journalManager.GetParentEntry(
      this.m_journalManager.GetTrackedEntry()
    )
  ) == this.m_bufferedQuest {
    this.m_journalManager.UntrackEntry();
  } else {
    this.m_journalManager.TrackEntry(this.m_bufferedQuest);
  }
  this.UpdateTrackerData();
}

public class QuestToggleListener {

  private let uiSystem: wref<UISystem>;

  public func SetUISystem(system: ref<UISystem>) -> Void {
    this.uiSystem = system;
  }

  protected cb func OnAction(
    action: ListenerAction,
    consumer: ListenerActionConsumer
  ) -> Bool {
    if ListenerAction.IsAction(action, n"QuestToggle") && ListenerAction.IsButtonJustReleased(action) {
      this.uiSystem.QueueEvent(new QuestToggleEvent());
    };
  }
}

@addField(PlayerPuppet)
private let QuestToggleListener: ref<QuestToggleListener>;

@wrapMethod(PlayerPuppet)
protected cb func OnGameAttached() -> Bool {
  wrappedMethod();
  this.QuestToggleListener = new QuestToggleListener();
  this.QuestToggleListener.SetUISystem(GameInstance.GetUISystem(this.GetGame()));
  this.RegisterInputListener(this.QuestToggleListener);
}

@wrapMethod(PlayerPuppet)
protected cb func OnDetach() -> Bool {
  wrappedMethod();
  this.UnregisterInputListener(this.QuestToggleListener);
  this.QuestToggleListener = null;
}
