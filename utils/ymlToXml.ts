#!/usr/bin/env bun

import { fileWrite } from './lib/file';
import { arraifyMappedObject } from './lib/mappedObject';
import {
  convertFastxmlObjectToXml,
  convertObjectToFastxmlObject,
} from './lib/xml';
import { convertYmlToObject, mapifyObject, yamlExpand } from './lib/yaml';

const deepRemoveNulls = (obj: any): any => {
  if (Array.isArray(obj)) {
    return obj.map(deepRemoveNulls);
  } else if (obj !== null && typeof obj === 'object') {
    return Object.fromEntries(
      Object.entries(obj)
        .filter(([_, value]) => value !== null)
        .map(([key, value]) => [key, deepRemoveNulls(value)]),
    );
  }
  return obj;
};

export const ymlToXml = async (
  yamlFilePathIn: string,
  xmlFilePathOut: string,
) => {
  try {
    const objectRaw = mapifyObject(
      await convertYmlToObject(yamlFilePathIn),
      '',
    );

    const expandedObject = { rootNode: yamlExpand(objectRaw).rootNode };
    const fastxmlObject = convertObjectToFastxmlObject(expandedObject);
    const arraifiedFastxmlObject = arraifyMappedObject(fastxmlObject);
    const finalObject = deepRemoveNulls(arraifiedFastxmlObject);

    const xmlData = convertFastxmlObjectToXml(finalObject);
    const result = '<?xml version="1.0"?>' + xmlData;

    if (xmlFilePathOut === '--') {
      console.log(result);
    } else {
      return await fileWrite(xmlFilePathOut, result + '\n');
    }
  } catch (error) {
    console.error('Conversion failed:', error);
  }
};
