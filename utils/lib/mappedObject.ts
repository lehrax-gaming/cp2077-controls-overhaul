const isKeyNumeric = (key: string): boolean => !isNaN(parseInt(key, 10));
const areAllKeysNumeric = (obj: { [key: string]: any }): boolean =>
  Object.keys(obj).every(isKeyNumeric);

export const deepMerge = (target: any, source: any): any => {
  Object.keys(source).forEach((key) => {
    target[key] =
      source[key] instanceof Object && key in target
        ? Array.isArray(target[key]) ||
          (areAllKeysNumeric(target[key]) &&
            areAllKeysNumeric(source[key]))
          ? mergeArrays(target[key], source[key])
          : deepMerge(target[key], source[key])
        : source[key];
  });

  return target;
};

const mergeArrays = (target: any, source: any): any[] => {
  const targetArray = objectToArray(target);
  const sourceArray = objectToArray(source);

  sourceArray.forEach((item, index) => {
    targetArray[index] = deepMerge(targetArray[index] || {}, item);
  });

  return targetArray;
};

const objectToArray = (obj: any): any[] => {
  if (Array.isArray(obj)) return obj;

  const arr: any = [];
  Object.keys(obj).forEach((key) => {
    const index = Number(key);
    if (!isNaN(index)) {
      arr[index] = obj[key];
    }
  });
  return arr;
};

export const arraifyMappedObject = (obj: any): any => {
  if (Array.isArray(obj)) {
    return obj.map((item) => arraifyMappedObject(item));
  } else if (obj && typeof obj === 'object') {
    const convertedObj = Object.entries(obj).reduce(
      (acc, [key, value]) => {
        acc[key] = arraifyMappedObject(value);
        return acc;
      },
      {} as { [key: string]: any },
    );
    if (areAllKeysNumeric(convertedObj)) {
      return Object.values(convertedObj);
    }
    return convertedObj;
  }
  return obj;
};
