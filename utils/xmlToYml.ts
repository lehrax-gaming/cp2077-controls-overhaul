#!/usr/bin/env bun

import { fileRead, fileWrite } from './lib/file';
import { convertXmlToObject } from './lib/xml';
import { mapifyObjectWithArrays, ymlDiff } from './lib/yaml';

export const xmlToYml = async (
  xmlFilePathIn: string,
  xmlFilePathOut = '--',
  factoryYamlFilePath: string | undefined = undefined,
) => {
  try {
    const objectRaw = await convertXmlToObject(xmlFilePathIn);
    const yamlData = mapifyObjectWithArrays(objectRaw);

    // produce delta vs factory config unless extracting factory now
    const deltify = async (
      ymlString: string,
      factoryYamlFilePath: string | undefined = undefined,
    ) => {
      if (factoryYamlFilePath === undefined) return ymlString;

      const factoryYaml = await fileRead(factoryYamlFilePath);
      return ymlDiff(factoryYaml, ymlString);
    };

    // todo: verify that no code injection through filename can happen here
    const yamlDataHeader = '# extracted from ' + xmlFilePathIn + '\n---\n';

    if (xmlFilePathOut === '--') {
      console.log(yamlDataHeader + yamlData);
    } else {
      return await fileWrite(
        xmlFilePathOut,
        yamlDataHeader + (await deltify(yamlData, factoryYamlFilePath)),
      );
    }
  } catch (error) {
    console.error('Conversion failed:', error);
  }
};
