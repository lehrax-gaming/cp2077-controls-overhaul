import { DIRS, EXT_WHITELIST, SOURCE_CODE } from './defaults';
import { join } from 'path';
import {
  dirExists,
  dirFiles,
  filenameExtGet,
  filenameExtSet,
} from './file';
import type { ScheduledAction } from './types';

export const scheduleAction = async (
  currentWorkingDirectory: string,
  operation: string,
  profile: string,
  profileLabel: string,
  releaseVersion: string,
  tagBranchCommit: string = '',
): Promise<ScheduledAction[]> => {
  const scheduledActions: ScheduledAction[] = [];
  const manifest = new Set<string>();

  if (profile.startsWith('factory,')) {
    const parts = profile.split(',');
    parts.shift();
    profile = parts.join(',');
  }

  const dirTo = join(
    currentWorkingDirectory,
    DIRS[operation]['to'],
    profileLabel,
  );

  // dirEmpty(dirTo);
  scheduledActions.push({
    operation: 'dirEmpty',
    from: dirTo,
    to: dirTo,
  });

  if (operation === 'ymlToXml') {
    if (!profile.startsWith('factory')) {
      profile = `factory,${profile}`;
    }

    const dirTmp = join(
      currentWorkingDirectory,
      DIRS['ymlToXml']['tmp'] as string,
      profileLabel,
    );

    // dirEmpty(dirTmp);
    scheduledActions.push({
      operation: 'dirEmpty',
      from: dirTmp,
      to: dirTmp,
    });

    const mixins =
      profile.indexOf(',') === -1
        ? [profile]
        : profile.split(',').filter((str) => str !== '');

    // check mixin parts of complex profile exist
    for (const mixin of mixins) {
      const from = join(
        currentWorkingDirectory,
        DIRS['ymlToXml']['from'],
        mixin,
      );
      if (!dirExists(from)) {
        throw (
          '[ERROR] Profile mixin is not present: ' +
          from +
          '\n[INFO] Use "- ' +
          mixin +
          '" operation if you are sure the name is correct to prepare source (factory is the default mixin)'
        );
      }

      const mixinFiles = await dirFiles(from);
      for (const filename of mixinFiles) {
        if (!EXT_WHITELIST.includes(filenameExtGet(filename))) continue;

        const filenameFrom = join(from, filename);
        const filenameTo = join(dirTmp, filename);

        const readme =
          filename === 'README.md'
            ? `README_ControlsOverhaul_${profileLabel}_v${releaseVersion}.md`
            : undefined;

        if (readme) {
          scheduledActions.push({
            operation: 'fileAppend',
            from: filenameFrom,
            to: join(dirTo, readme),
          });

          // newline between mixin README text nodes
          scheduledActions.push({
            operation: 'fileAppend',
            from: '',
            to: join(dirTo, readme),
            text: '\n',
          });

          if (tagBranchCommit !== '') {
            // https://gitlab.com/lehrax-gaming/cp2077-controls-overhaul/-/tree/1eff95420118a26876bc539a82c9aaf8d3993227/mixins/factory
            scheduledActions.push({
              operation: 'fileAppend',
              from: '',
              text: `\n${SOURCE_CODE['repo']}/${SOURCE_CODE['tree']}/${
                tagBranchCommit
              }/${SOURCE_CODE['path']}/${mixin}\n\n`,
              to: join(dirTo, readme),
            });
          }
          continue;
        }

        manifest.add(filename);

        scheduledActions.push({
          operation: filename.endsWith('.yml')
            ? 'ymlDeepMerge'
            : 'fileCopy',
          from: filenameFrom,
          to: filenameTo,
        });
      }
    }

    // now as merging is completed, convert merged source into xml

    const fromTmp = join(
      currentWorkingDirectory,
      DIRS['ymlToXml']['tmp'] as string,
      profileLabel,
    );

    for (const filename of manifest) {
      const filenameFrom = join(fromTmp, filename);
      const filenameTo = join(dirTo, filename);

      const willConvert = filename.endsWith('.yml');

      scheduledActions.push({
        operation: willConvert ? 'ymlToXml' : 'fileCopy',
        from: filenameFrom,
        to: willConvert ? filenameExtSet(filenameTo, 'xml') : filenameTo,
      });
    }
  } else {
    // profile extraction to source
    const from = join(
      currentWorkingDirectory,
      DIRS['xmlToYml']['from'],
      profileLabel,
    );
    const factoryFrom = join(
      currentWorkingDirectory,
      DIRS['xmlToYml']['to'],
      'factory',
    );
    if (!dirExists(from))
      throw '[ERROR] Profile mixin is not present: ' + from;

    const profileFiles = await dirFiles(from);
    for (const filename of profileFiles) {
      if (!EXT_WHITELIST.includes(filenameExtGet(filename))) continue;

      const filenameFrom = join(from, filename);
      const filenameTo = join(dirTo, filename);

      const readme =
        filename.startsWith('README_') && filename.endsWith('.md')
          ? join(dirTo, 'README.md')
          : undefined;

      if (readme) {
        scheduledActions.push({
          operation: 'fileAppend',
          from: filenameFrom,
          to: readme,
        });
        continue;
      }

      scheduledActions.push({
        operation: filename.endsWith('.xml') ? operation : 'fileCopy',
        from: filenameFrom,
        to: filenameExtSet(filenameTo, 'yml'),

        // dirty hack to provide factory profile, rework
        text:
          profile === 'factory'
            ? undefined
            : filenameExtSet(join(factoryFrom, filename), 'yml'),
      });
    }
  }

  return scheduledActions;
};
