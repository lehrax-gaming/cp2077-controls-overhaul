#!/usr/bin/env bun

import { dirEmpty, fileAppend, fileCopy, fileWrite } from './lib/file';
import { xmlToYml } from './xmlToYml';
import { yamlRead, ymlDiff } from './lib/yaml';
import { ymlDeepMerge } from './ymlDeepMerge';
import { ymlToXml } from './ymlToXml';

const main = async () => {
  const operation = Bun.argv[2];
  const positionalArgs = Bun.argv.slice(3);

  switch (operation) {
    case 'fileAppend':
      await fileAppend(positionalArgs[0], positionalArgs[1]);
      break;
    case 'fileCopy':
      await fileCopy(positionalArgs[0], positionalArgs[1]);
      break;
    case 'dirEmpty':
      await dirEmpty(positionalArgs[0]);
      break;
    case 'xmlToYml':
      await xmlToYml(positionalArgs[0], positionalArgs[1]);
      break;
    case 'ymlToJson':
      const parsed = await yamlRead(positionalArgs[0]);
      const stringified = JSON.stringify(parsed, null, 2);
      if (positionalArgs[1]) {
        await fileWrite(positionalArgs[1], stringified);
      } else {
        console.log(stringified);
      }
      break;
    case 'ymlDeepMerge':
      await ymlDeepMerge(...positionalArgs);
      break;
    case 'ymlDelta':
      ymlDiff(positionalArgs[0], positionalArgs[1]);
      break;
    case 'ymlToXml':
      await ymlToXml(positionalArgs[0], positionalArgs[1]);
      break;
  }
};

main();
