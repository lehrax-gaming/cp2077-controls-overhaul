import { arraifyMappedObject, deepMerge } from './mappedObject';
import { convertFastxmlObjectToObject } from './xml';
import { fileRead, fileWrite } from './file';
import YAML from 'yaml';

const REPLACEABLE = '__replaceme__';

// Function to parse YAML to JSON
export const parseYaml = (yamlData: string): any => YAML.parse(yamlData);

export const yamlRead = async (path: string) =>
  parseYaml(await fileRead(path));

// Function to convert YAML to XML
export const mergeYaml = async (
  yamlFilePaths: string[],
): Promise<string> => {
  try {
    let jsonData = {};
    for (const yamlFilePath of yamlFilePaths) {
      // Read YAML file
      const yamlData = await (async function () {
        return await fileRead(yamlFilePath).catch((_) => 'rootNode: []');
      })();

      // Parse YAML to JSON
      let newJsonData = parseYaml(yamlData);

      // Deep merge on top of existing data
      jsonData = deepMerge(jsonData, newJsonData);
    }

    // Convert JSON to YAML
    // return YAML.stringify(jsonData, { defaultKeyType: 'PLAIN' });
    return mapifyObjectWithArrays(jsonData);
  } catch (error: any) {
    throw '[ERROR] ' + error.message;
  }
};

export const objectWithArraysToTaggedMap = (
  obj: any,
  pad = REPLACEABLE,
): any => {
  if (Array.isArray(obj)) {
    return obj.reduce(
      (acc, item, index) => {
        acc[pad + index] = objectWithArraysToTaggedMap(item, pad);
        return acc;
      },
      {} as { [key: number]: any },
    );
  } else if (obj && typeof obj === 'object') {
    return Object.entries(obj).reduce(
      (acc, [key, value]) => {
        acc[key] = objectWithArraysToTaggedMap(value, pad);
        return acc;
      },
      {} as { [key: string]: any },
    );
  }
  return obj;
};

export const mapifyObject = (jsObject: any, pad = REPLACEABLE) => {
  const taggedMap = objectWithArraysToTaggedMap(jsObject, pad);
  return convertFastxmlObjectToObject(taggedMap);
};

export const mapifyObjectWithArrays = (jsObject: any) => {
  const taggedMap = objectWithArraysToTaggedMap(jsObject);
  const fastxmlObject = convertFastxmlObjectToObject(taggedMap);

  return YAML.stringify(fastxmlObject).replaceAll(REPLACEABLE, '');
};

export const convertYmlToObject = async (path: string) =>
  arraifyMappedObject(await yamlRead(path));

export const yamlExpand = (obj: any): any => {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }

  const result = Array.isArray(obj) ? [] : {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (key === '<<') {
        const entries = obj[key];
        for (const subKey in entries) {
          if (entries.hasOwnProperty(subKey)) {
            (result as any)[subKey] = yamlExpand(entries[subKey]);
          }
        }
      } else {
        (result as any)[key] = yamlExpand(obj[key]);
      }
    }
  }

  return result;
};

export const ymlDiff = (yaml1: string, yaml2: string) => {
  const obj1 = YAML.parse(yaml1);
  const obj2 = YAML.parse(yaml2);

  const findDelta = (obj1: any, obj2: any) => {
    let delta: any = {};

    for (const key in obj1) {
      if (obj1.hasOwnProperty(key)) {
        if (obj2.hasOwnProperty(key)) {
          if (typeof obj1[key] === 'object' && !Array.isArray(obj1[key])) {
            const nestedDelta = findDelta(obj1[key], obj2[key]);
            if (Object.keys(nestedDelta).length > 0) {
              delta[key] = nestedDelta;
            }
          } else if (obj1[key] !== obj2[key]) {
            delta[key] = obj2[key];
          }
        } else {
          delta[key] = null;
        }
      }
    }

    for (const key in obj2) {
      if (obj2.hasOwnProperty(key) && !obj1.hasOwnProperty(key)) {
        delta[key] = obj2[key];
      }
    }

    return delta;
  };

  const pureDiff = findDelta(obj1, obj2);
  const arraifiedDiff = arraifyMappedObject(pureDiff);
  const numerifiedDiff = YAML.stringify(
    mapifyObject(arraifiedDiff, REPLACEABLE),
    {
      nullStr: '~',
    },
  ).replaceAll(REPLACEABLE, '');

  return numerifiedDiff;
};
