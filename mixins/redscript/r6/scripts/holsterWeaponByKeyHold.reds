// inspired by original work of djkovrik: https://github.com/djkovrik/CP77Mods/blob/master/src/Holster%20By%20Hold/r6/scripts/holserByHold.reds
@wrapMethod(UpperBodyEventsTransition)
protected final func UpdateSwitchItem(
  timeDelta: Float,
  stateContext: ref<StateContext>,
  scriptInterface: ref<StateGameScriptInterface>
) -> Bool {
  if scriptInterface.IsActionJustHeld(n"HolsterWeapon") {
    if !StatusEffectSystem.ObjectHasStatusEffectWithTag(
      scriptInterface.executionOwner, n"FirearmsNoUnequip"
    ) && (
      DefaultTransition.IsHeavyWeaponEquipped(scriptInterface) || DefaultTransition.HasMeleeWeaponEquipped(scriptInterface) || DefaultTransition.IsRangedWeaponEquipped(scriptInterface) || DefaultTransition.HasRightWeaponEquipped(scriptInterface)
    ) {
      this.SendEquipmentSystemWeaponManipulationRequest(
        scriptInterface,
        EquipmentManipulationAction.UnequipWeapon
      );
      this.ResetEquipVars(stateContext);
      return true;
    } else {
      this.SendEquipmentSystemWeaponManipulationRequest(
        scriptInterface,
        EquipmentManipulationAction.RequestLastUsedOrFirstAvailableWeapon
      );
      this.ResetEquipVars(stateContext);
      return true;
    };
  };

  return wrappedMethod(timeDelta, stateContext, scriptInterface);
}
