import { appendFile, cp, mkdir, readdir, rm } from 'fs/promises';
import { dirname } from 'path';
import { statSync } from 'fs';

export const dirExists = (path: string): boolean => {
  const dir = Bun.file(path);

  if (!dir.exists()) {
    return false;
  }

  try {
    const stats = statSync(path);
    return stats.isDirectory();
  } catch {
    return false;
  }
};

export const dirCreate = async (path: string) =>
  await mkdir(path, { recursive: true });

export const dirDelete = async (path: string) => {
  try {
    await rm(path, { recursive: true, force: true });
    return true;
  } catch {
    return false;
  }
};

export const dirEmpty = async (path: string) => {
  await dirDelete(path);
  return await dirCreate(path);
};

export const dirFiles = async (path: string) =>
  await readdir(path, { recursive: true });

export const fileRead = async (path: string) =>
  await Bun.file(path).text();

export const fileWrite = async (path: string, content: string) =>
  await Bun.write(path, content);

export const filenameExtGet = (path: string) =>
  path.split('/').pop()?.split('.').pop() || '';

export const fileCopy = async (pathFrom: string, pathTo: string) => {
  try {
    await mkdir(dirname(pathTo), { recursive: true });
    return await cp(pathFrom, pathTo);
  } catch (err) {
    throw `[ERROR] Copying file failed: ${err}`;
  }
};

export const fileAppend = async (
  from: string,
  to: string,
  fromText = false,
) => {
  try {
    const content = fromText ? from : await fileRead(from);
    return await appendFile(to, content);
  } catch {
    return fileCopy(from, to);
  }
};

export const filenameExtSet = (path: string, extension: string) =>
  `${path.split('.').slice(0, -1).join('.')}.${extension}`;
