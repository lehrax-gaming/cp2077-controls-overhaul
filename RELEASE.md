## 2.20.7 - 2024-12-29

### Added

- RMB to close map

### Changed

- Space to track destination on map

### Deprecated

- n/a

### Removed

- W/S for dialogue options selection (arrow keys and mouse wheel still work)

### Fixed

- walking keys (W/S) will not affect dialogue response selection

### Security

- n/a
