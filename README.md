# CP2077 - Controls overhaul

Preface: I am an old-school gamer appreciating the classics like HL-1 and HL-2 where E would be action and everything was crystal-clear. Basically this control scheme modification is ought to reduce the inconsistency in key binds and make Cyberpunk into a game with more traditional controls, hence...

=== CONTROLS OVERHAUL ===

Every mapping pattern I set here is either common across other games or justified / easy to comprehend.

All the menus such as phone, radio, crafting/trading/scrapping, dialogues etc are overhauled to understand mouse scroll same as arrow keys in addition to default behaviour.

Overhauled controls are described in mixin READMEs. Merged README can be found in release artifacts.

## Dev requirements

- bun

```sh
bun install # install npm reqirements
./prepare - # unpack factory profile
```

## Mixins

Directories inside `mixins` represent mixins and are designed to be easily mergeable when same xpath is used. `rootNode` object holds all declarations in sub-objects. For ease of overriding array items those can be declared with numeric indexes instead of array notation. Apart from that mixins support usual YAML features such as anchors etc. Removing attributes is done by setting them to null (`~`).


## Preparing profile(s)

You can provide desired profile as a comma-separated list of mixins. Since every profile is based on default [`factory`] profile, it is not required to have it as part of this variable. Example: `factory,redscript` is the same as `redscript`.

You can optionally give your profile custom names (after `=` sign). By default profile is named as the list of mixins.

```sh
# when starting to work with new XML files
./prepare.ts -  # only needed one time after dropping new sources into dist/factory

# [optional] recreate factory profile dist
./prepare.ts factory

# [optional] optimise produced XML files
./utils/formatter/formatAll.sh # <- requires python-lxml installed: `pip install lxml`

# provide a list of profiles (comma separated mixins)
# LABEL is optional but nice to have if you want to have a custom profile name
./prepare.ts factory=defaults_from_CDPR vanillaplus,redscript=my_custom_profile_YAY

# [optional] optimise produced XML files
./utils/formatter/formatAll.sh
```


## Installation

1. Use your favourite mod manager or just drop the r6 in the game root directory.
2. Go into Settings / Key bindings and reset to DEFAULTS.


## Remarks

If you find any issue or have a valuable suggestion, feel free to inform me about it on [NexusMods' page](https://www.nexusmods.com/cyberpunk2077/mods/3060) or [GitLab project page](https://gitlab.com/lehrax-gaming/cp2077-controls-overhaul).

Enjoy!


## Endorsements

- CDPR
- modding community
- [djkovrik](https://github.com/djkovrik/CP77Mods/)
