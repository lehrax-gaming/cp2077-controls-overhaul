# redscript

This mixin contains the following Redscript functions:
- holstering weapon by Reload long press (like in Fallout 4)
- toggling last quest active/inactive
