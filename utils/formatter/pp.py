#!/usr/bin/env python3

import sys
import lxml.etree as ET
from xml.dom.minidom import parseString

xslt_transformation = '''<?xml version="1.0" ?>
  <xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  >

    <xsl:output indent="yes" method="xml" />
    <xsl:strip-space elements="*" />

    <xsl:template match="/">
      <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="*">
      <xsl:copy>
        <!-- Sort the attributes by name. -->
        <xsl:for-each select="@*">
          <xsl:sort select="name( . )" />
          <xsl:copy />
        </xsl:for-each>
        <xsl:apply-templates />
      </xsl:copy>
    </xsl:template>

    <xsl:template match="text()|comment()|processing-instruction()">
      <xsl:copy />
    </xsl:template>

  </xsl:stylesheet>''';

def pretty_print_xml(xml_file, output_file):
  try:
    dom = ET.parse(xml_file)

    # Apply xslt
    xslt_tree = ET.XML(xslt_transformation)
    transform = ET.XSLT(xslt_tree)
    newdom = transform(dom)

    # Pretty print
    transformed_str = ET.tostring(newdom, xml_declaration=True, encoding='UTF-8')

    pretty_xml_str = parseString(transformed_str).toprettyxml(indent="  ")
    pretty_xml_str = "\n".join([line for line in pretty_xml_str.split("\n") if line.strip()])

    # Write the pretty printed XML to the output file (+ final newline)
    with open(output_file, 'w') as f:
      f.write(pretty_xml_str + "\n")

  except ET.ParseError as e:
    print(f"Error parsing XML file: {e}")
    sys.exit(1)

# Check command-line arguments
if len(sys.argv) != 3:
  print("Usage: python3 " + sys.argv[0] + " <input_xml_file> <output_xml_file>")
  sys.exit(1)

input_file = sys.argv[1]
output_file = sys.argv[2]

# Pretty print the XML and save to the output file
pretty_print_xml(input_file, output_file)
