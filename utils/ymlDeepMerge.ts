#!/usr/bin/env bun

import { fileWrite } from './lib/file';
import { mergeYaml } from './lib/yaml';

export const ymlDeepMerge = async (...yamlFragmentPaths: string[]) => {
  const yamlMergedOutputPath = yamlFragmentPaths.pop() as string;
  yamlFragmentPaths.unshift(yamlMergedOutputPath);
  const mergeOrder = yamlFragmentPaths;

  try {
    const result = await mergeYaml(mergeOrder);

    if (yamlMergedOutputPath === '--') {
      console.log(result);
    } else {
      return await fileWrite(yamlMergedOutputPath, result);
    }
  } catch (error) {
    console.error('Merge failed:', error);
  }
};

/*
const main = async () => {
  // Get input file path from CLI arguments
  const yamlFragmentPaths: string[] = Bun.argv.slice(2);

  if (yamlFragmentPaths.length < 2) {
    console.error(
      "Please provide the YAML file paths as CLI arguments (last file path is output).",
    );
    return;
  }

  await ymlDeepMerge(yamlFragmentPaths);
};

main();
*/
