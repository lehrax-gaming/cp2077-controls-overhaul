#!/usr/bin/env bun

import {
  dirEmpty,
  fileAppend,
  fileCopy,
  fileRead,
} from './utils/lib/file';
import { scheduleAction } from './utils/lib/scheduleAction';
import type { ScheduledAction } from './utils/lib/types';
import { xmlToYml } from './utils/xmlToYml';
import { ymlDeepMerge } from './utils/ymlDeepMerge';
import { ymlToXml } from './utils/ymlToXml';

const DEBUG = Boolean(process.env.DEBUG);

const help = () => {
  console.log(Bun.argv[1], '[-] [profiles]');
  console.log('[INFO] Provide space separated list of profiles.');
  console.log('[INFO] Use `-` as first argument if extracting source');
};

//
const main = async () => {
  const cliArgs = Bun.argv.slice(2);
  const cwd = process.cwd();

  if (cliArgs.length === 0) {
    help();
    return;
  }

  const schedule: ScheduledAction[] = [];

  const operation =
    await // dash as first argument switches the operation to source extraction
    (
      cliArgs[0] === '-' // would be nice to use '--', but Bun ignores it
        ? async () => {
            if (
              cliArgs.indexOf('factory') === -1 &&
              cliArgs.length === 1
            ) {
              cliArgs[0] = 'factory';
            } else {
              cliArgs.shift();
            }
            return 'xmlToYml';
          }
        : () => 'ymlToXml'
    )();

  const profiles = cliArgs;

  const tagBranchCommit = process.env.TAG_BRANCH_COMMIT || '';

  const releaseNotes = await fileRead('RELEASE.md');
  const releaseVersion =
    releaseNotes.split('\n')[0].split(' ')[1] || 'STAGING';

  for (let profile of profiles) {
    const profileParts = profile.split('=');
    const mixins = profileParts[0];
    const label = profileParts[1] ?? mixins;

    const newActions = await scheduleAction(
      cwd,
      operation,
      mixins,
      label,
      releaseVersion,
      tagBranchCommit,
    );
    for (const newAction of newActions) {
      schedule.push(newAction);
    }
  }

  for (const action of schedule) {
    if (DEBUG) {
      console.log(action);
      console.log(
        `./utils/testOperation.ts ${
          action.operation
        } ${action.text || ''} ${action.from || ''} ${action.to || ''}\n`.replaceAll(
          '  ',
          ' ',
        ),
      );
    }

    switch (action.operation) {
      case 'fileAppend':
        await fileAppend(
          action.text || action.from,
          action.to,
          action.text !== undefined,
        );
        break;
      case 'fileCopy':
        await fileCopy(action.from, action.to);
        break;
      case 'dirEmpty':
        await dirEmpty(action.from);
        break;
      case 'xmlToYml':
        await xmlToYml(action.from, action.to, action.text); // todo: rewrite
        break;
      case 'ymlDeepMerge':
        await ymlDeepMerge(action.from, action.to);
        break;
      case 'ymlToXml':
        await ymlToXml(action.from, action.to);
        break;
    }
  }
};

main();
