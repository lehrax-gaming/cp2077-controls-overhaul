import type { DirDeclaration } from './types';

export const DIRS: Record<string, DirDeclaration> = {
  xmlToYml: {
    from: 'dist',
    to: 'mixins',
  },
  ymlToXml: {
    from: 'mixins',
    to: 'dist',
    tmp: 'tmp',
  },
};

export const EXT_WHITELIST = ['md', 'reds', 'txt', 'xml', 'yml'];

export const SOURCE_CODE = {
  repo: 'https://gitlab.com/lehrax-gaming/cp2077-controls-overhaul',
  tree: '-/tree',
  tag_branch_commit: undefined,
  path: 'mixins',
  dir: undefined,
};
