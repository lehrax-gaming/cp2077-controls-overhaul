#!/bin/sh

WORKDIR=${1:-$(pwd)}
PRETTYPRINT=${2:-$(dirname "$0")/pp.py}

cd "$WORKDIR" || exit 1
echo "Directory: $WORKDIR"

mkdir -p /tmp/prettyprint &> /dev/null
find . -type f -name "*.xml" | while read file; do
  tempFile="/tmp/prettyprint/$file"
  mkdir -p $(dirname "$tempFile")

  python "$PRETTYPRINT" "$file" "$tempFile"

  printf "     File: '$file' "
  if [ -f "$tempFile" ]; then
    mv "$tempFile" "$file"
    echo 'formatted!'
  else
    echo 'skipped...'
  fi
done
rm -r /tmp/prettyprint &> /dev/null
