export interface MappedObject {
  rootNode: object;
}

export interface DirDeclaration {
  from: string;
  to: string;
  tmp?: string;
}

export interface ScheduledAction {
  operation: string;
  from: string;
  text?: string;
  to: string;
}
