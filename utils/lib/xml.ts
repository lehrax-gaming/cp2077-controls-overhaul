import { fileRead } from './file';
import type { MappedObject } from './types';
import { XMLBuilder, XMLParser } from 'fast-xml-parser';

export const convertFastxmlObjectToXml = (
  jsObject: MappedObject,
): string => {
  try {
    return new XMLBuilder({
      attributeNamePrefix: '$',
      commentPropName: '--',
      format: true,
      ignoreAttributes: false,
      indentBy: '  ',
      preserveOrder: true,
      suppressBooleanAttributes: false,
      suppressEmptyNode: true,
      textNodeName: '$',
    }).build(jsObject.rootNode);
  } catch (error: any) {
    throw `[ERROR] ${error.message}`;
  }
};

export const convertXmlToObject = async (
  xmlFilePath: string,
): Promise<MappedObject> => {
  try {
    return {
      rootNode: new XMLParser({
        attributeNamePrefix: '$',
        commentPropName: '--',
        ignoreAttributes: false,
        ignoreDeclaration: true, // strip <?xml version="1.0"?>
        preserveOrder: true,
        textNodeName: '$',
      }).parse(await fileRead(xmlFilePath)),
    };
  } catch (error: any) {
    throw `[ERROR] ${error.message}`;
  }
};

interface JSONObject {
  [key: string]: any;
}

export const convertFastxmlObjectToObject = (
  jsonObject: JSONObject,
): JSONObject => {
  const result: JSONObject = {};

  const distributeProperties = (
    parent: JSONObject,
    key: string,
    obj: JSONObject,
  ): void => {
    if (obj[':@']) {
      const propHolderKey = Object.keys(obj).find(
        (k) => /^[a-zA-Z]/.test(k) && k !== ':@',
      );
      if (propHolderKey) {
        if (!parent[key][propHolderKey]) {
          parent[key][propHolderKey] = {};
        }
        Object.assign(parent[key][propHolderKey], obj[':@']);
        delete obj[':@'];
      }
    }

    for (const [childKey, childValue] of Object.entries(obj)) {
      if (typeof childValue === 'object' && childValue !== null) {
        distributeProperties(obj, childKey, childValue);
      }
    }
  };

  const processObject = (
    obj: JSONObject,
    parent: JSONObject,
    key: string,
  ): void => {
    const properties: JSONObject = {};
    const children: JSONObject = {};

    for (const [childKey, childValue] of Object.entries(obj)) {
      if (childKey === ':@') {
        Object.assign(properties, childValue);
      } else {
        children[childKey] = childValue;
      }
    }

    if (Object.keys(properties).length > 0) {
      if (!parent[key]) {
        parent[key] = {};
      }
      const propHolderKey = Object.keys(children).find((k) =>
        /^[a-zA-Z]/.test(k),
      );
      if (propHolderKey) {
        parent[key][propHolderKey] = properties;
      }
    }

    Object.assign(parent[key] || {}, children);

    for (const [childKey, childValue] of Object.entries(children)) {
      if (typeof childValue === 'object' && childValue !== null) {
        processObject(childValue, parent[key] || {}, childKey);
      }
    }
  };

  for (const [key, value] of Object.entries(jsonObject)) {
    if (typeof value === 'object' && value !== null) {
      result[key] = {};
      processObject(value, result, key);
      distributeProperties(result, key, result[key]);
    } else {
      result[key] = value;
    }
  }

  return result;
};

export const convertObjectToFastxmlObject = (
  jsonObject: JSONObject,
): JSONObject => {
  const processObject = (
    obj: JSONObject,
    parent: JSONObject | null = null,
  ): JSONObject => {
    const result: JSONObject = {};
    const properties: JSONObject = {};

    for (const [key, value] of Object.entries(obj)) {
      if (key.startsWith('$')) {
        properties[key] = value;
      } else if (
        typeof value === 'object' &&
        value !== null &&
        !Array.isArray(value)
      ) {
        result[key] = processObject(value, result);
      } else {
        result[key] = value;
      }
    }

    if (Object.keys(properties).length > 0) {
      if (!parent) {
        return { ':@': properties, ...result };
      }
      if (!parent[':@']) {
        parent[':@'] = {};
      }
      Object.assign(parent[':@'], properties);
    }

    return result;
  };

  const transform = (
    obj: JSONObject,
    parent: JSONObject | null = null,
  ): JSONObject => {
    const result: JSONObject = {};

    for (const [key, value] of Object.entries(obj)) {
      if (typeof value === 'object' && value !== null && key !== '--') {
        result[key] = transform(value, result);
      } else {
        result[key] = value;
      }
    }

    if (parent && Object.keys(result).some((k) => k.startsWith('$'))) {
      if (!parent[':@']) {
        parent[':@'] = {};
      }
      for (const key of Object.keys(result)) {
        if (key.startsWith('$')) {
          parent[':@'][key] = result[key];
          delete result[key];
        }
      }
    }

    return result;
  };

  const result: JSONObject = {};

  for (const [key, value] of Object.entries(jsonObject)) {
    result[key] = transform(value);
  }

  return result;
};
