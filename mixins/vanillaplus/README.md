# vanillaplus

This mixin makes the following changes to the way vanilla game behaves:
- mouse wheel for dialogue selection
- action key is E pretty much everywhere in the game
- rewinding braindance with Z and fast-forwarding with X
- (tele)phone related actions with T (pick up call, text, task)
- vehicle related actions with V (long: toggle headlights, short: switch camera)
- Berserk (iconic cyberware) with B
- fists/monowire with 0
